{ compiler ? "ghc844", pkgs ? import <nixpkgs> {}, withHoogle ? false }:

let

  haskellPackages = pkgs.haskell.packages.${compiler};
  haskellPackagesWithHoogle =
    if withHoogle
    then haskellPackages.override {
      overrides = (self: super: {
        ghc = super.ghc // { withPackages = super.ghc.withHoogle; };
        ghcWithPackages = self.ghc.withPackages;
      });
    }
    else haskellPackages;
  drv = haskellPackagesWithHoogle.callCabal2nix "hs-md-preview" ./. {};

in
  {
    hs-md-preview = drv;
    hs-md-preview-shell = haskellPackages.shellFor {
      packages = p: [drv];
      buildInputs = [
        haskellPackages.cabal-install
        haskellPackages.hlint
        haskellPackages.ghcid
        haskellPackages.hoogle
        haskellPackages.hfmt
        pkgs.my-haskell-nvim
      ];
    };
  }
