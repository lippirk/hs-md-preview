# hs-md-preview

A quick and (very) dirty markdown previewer

#### 27th Jan 2019 - Updated to build with nix

Development:

 - Haskell - `$ nix-shell`

 - JS - haven't worked out how npm works with nix yet. For now, do it the old way!

 ```
 $ cd client
 $ nix-shell -p nodejs # easiest way to create ad-hoc environment with npm
 $ npm i
 $ npm run build
 ```

Install:

 - Make sure client has been generated.
 - `$ nix-env -f default.nix -iA hs-md-preview`

#### 10 Jan 2019 - Original

Usage:

```
$ ./build.sh ~/.local/bin     # or other dir of your choice which is in PATH

$ cd path/to/md/directory

$ hs-md-preview my-md-file.md
```

Tested on xubuntu 18.04
