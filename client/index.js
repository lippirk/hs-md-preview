import 'babel-polyfill';

const serverUrl = "localhost:5678";

const client = new WebSocket(`ws://${serverUrl}`);

const getMarkdown = async () => {

  let md = undefined;

  try {
    const resp = await fetch('/content');
    md = await resp.text();
    console.log('md request successful');
  } catch (err) {
    console.log('md request unsuccessful:');
    console.log(err);
  }

  return md;
};

client.onopen = () => {
  console.log(`CONNECTED to ${serverUrl}`);
  mkRequest("update");
};

const mkRequest = async rqType => {
  switch (rqType) {
    case "update":
      const md = await getMarkdown();

      if (md !== undefined) {
        document.getElementById('content').innerHTML = md;
        console.log("updated md");
      }
      break;
  default:
    console.log(`unknown request type: ${rqType}`);
  }
};

client.onmessage = async e => {
  const message = e.data;
  console.log(`received message: ${message}`);
  await mkRequest(message);
};
