{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Main
  ( main
  ) where

import           ClassyPrelude                  hiding (getArgs)
import           Control.Concurrent             (threadDelay)
import qualified Data.ByteString                as B
import qualified Data.ByteString.Char8          as BC8
import qualified Data.ByteString.Lazy           as BL
import qualified Data.List                      as List
import           Network.HTTP.Types             (status200)
import           Network.Wai                    (Application, rawPathInfo,
                                                 responseLBS)
import qualified Network.Wai.Handler.Warp       as Warp
import           Network.Wai.Handler.WebSockets (websocketsOr)
import qualified Network.WebSockets             as WS
import           System.Directory               (doesFileExist,
                                                 getCurrentDirectory,
                                                 listDirectory)
import           System.Environment             (getArgs)
import qualified System.FSNotify                as FSN
import           Text.Pandoc                    (Extension (Ext_auto_identifiers, Ext_backtick_code_blocks, Ext_fenced_code_attributes, Ext_footnotes),
                                                 Pandoc, PandocIO)

import qualified Data.FileEmbed                 as FE
import qualified Text.Pandoc                    as P

indexHtml :: B.ByteString
indexHtml = $(FE.embedFile "client/dist/index.html")

indexJs :: B.ByteString
indexJs = $(FE.embedFile "client/dist/client.e31bb0bc.js")

port :: Int
port = 5678

updateMsg :: Text
updateMsg = "update"

main :: IO ()
main = do
  putStrLn $ "Visit http://localhost:" <> (fromString . show $ port)
  markdownFilePath <- getMarkdownFilePath =<< getArgs
  Warp.run 5678 (app markdownFilePath)
  where
    getMarkdownFilePath :: [String] -> IO FilePath
    getMarkdownFilePath [fileName] = do
      currentDir <- getCurrentDirectory
      let markdownFilePath = currentDir <> "/" <> fileName
      doesMarkdownFileExist <- doesFileExist markdownFilePath
      if doesMarkdownFileExist
        then return markdownFilePath
        else error $ markdownFilePath <> " does not exist."
    getMarkdownFilePath _ = error "Pass exactly one argument"

app :: FilePath -> Application
app pathToMarkdownFile =
  websocketsOr
    WS.defaultConnectionOptions
    (webSocketsApp pathToMarkdownFile)
    (httpApp pathToMarkdownFile)

webSocketsApp :: FilePath -> WS.ServerApp
webSocketsApp mdFilePath pendingRequest = do
  conn <- WS.acceptRequest pendingRequest
  whenFileUpdates
    mdFilePath
    (putStrLn "md file saved" >> WS.sendTextData conn updateMsg)

httpApp :: FilePath -> Application
httpApp pathToMarkdownFile rq rsp
  | path == "/" = bytestring . fromStrict $ indexHtml
  | "/client" `B.isPrefixOf` path = bytestring . fromStrict $ indexJs
  | path == "/content" = do
    markdownAsHtml <- markdownToHtml pathToMarkdownFile
    case markdownAsHtml of
      Nothing   -> bytestring "Could not parse md file into html."
      Just html -> text html
  | otherwise = bytestring "Error!"
  where
    path = rawPathInfo rq
    bytestring message = rsp $ responseLBS status200 [] message
    text message =
      rsp $ responseLBS status200 [] (fromStrict . encodeUtf8 $ message)

markdownToHtml :: FilePath -> IO (Maybe Text)
markdownToHtml fp = do
  eHtml <-
    P.runIO $ do
      fileContents <- decodeUtf8 <$> P.readFileStrict fp
      ast <- P.readMarkdown readerOpts fileContents
      P.writeHtml5String P.def ast
  case eHtml of
    Left e     -> (putStrLn . fromString . show $ e) >> return Nothing
    Right html -> return . Just $ html
  where
    extensions =
      P.extensionsFromList
        [ Ext_fenced_code_attributes
        , Ext_backtick_code_blocks
        , Ext_footnotes
        , Ext_auto_identifiers
        ]
    readerOpts = P.def {P.readerExtensions = extensions}

whenFileUpdates :: FilePath -> IO () -> IO ()
whenFileUpdates fp action = do
  FSN.withManager $ \mgr -> do
    FSN.watchDir mgr "." fltr (const action)
    forever $ threadDelay 1000000
  putStrLn "Finished watching file"
  where
    fltr :: FSN.Event -> Bool
    fltr (FSN.Modified fp _ _) = True
    fltr _                     = False
